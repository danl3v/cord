import os, pos, simulator

class Tester:
    def __init__(self, sdk_version, project_name, project_path):
        can_run = True
        can_run = self.setup_sdk_version(sdk_version) and can_run
        can_run = self.setup_project_name(project_name) and can_run
        can_run = self.setup_project_path(project_path, project_name) and can_run
        
        points_of_sale = pos.PointsOfSale()
        self.points_of_sale = points_of_sale
        
        self.can_run = can_run
            
    def setup_project_name(self, project_name):
        if not project_name:
            print "You must choose a project name."
            return False
        else:
            self.project_name = project_name
            return True

    def setup_sdk_version(self, user_sdk_version):
        sdk_list = simulator.valid_sdks()
        if len(sdk_list) == 0:
            print "Error: No valid sdks. Try building the project once before running the loc script"
            return False
        elif not user_sdk_version:
            self.sdk_version = user_sdk_version
            return True
        elif not user_sdk_version in sdk_list:
            print "Error: You must choose a valid sdk version. Here are yours:"
            for sdk in sdk_list:
                print sdk
            return False
        else:
            self.sdk_version = user_sdk_version
            return True
    
    def setup_project_path(self, project_path, project_name):
        if not project_path:
            print "You must choose a project path."
            return False
        else:
            if project_path[-1] != "/":
                project_path += "/"
            self.project_path = project_path + project_name + ".xcodeproj"
            return True
    
    def print_report(self):
        os.system("clear")
        print
        print "-----------------------"
        print "PROJECT: " + self.project_name
        if self.sdk_version:
            print "SDK: " + self.sdk_version
        print "-----------------------"
        for point_of_sale in self.points_of_sale.all_points_of_sale:
            if point_of_sale["comment"]:
                print "Notes for " + point_of_sale["name"] + " (" + point_of_sale['language'] + "_" + point_of_sale['locale'] + ")"
                print point_of_sale["comment"]
                print "-----------------------"
                print

    
    def run(self):
        if not self.can_run:
            return
    
        current_point_of_sale = None
        next_point_of_sale = None
        while True:
            os.system("clear")
            
            index = -1
            if current_point_of_sale:
                index = self.points_of_sale.all_points_of_sale.index(current_point_of_sale)
            if index+1 < (len(self.points_of_sale.all_points_of_sale) - 1):
                next_point_of_sale = self.points_of_sale.all_points_of_sale[index+1]
            else:
                next_point_of_sale = None
        
            # print the tool name
            if not current_point_of_sale:
                print "LANGUAGE TESTING TOOL"
                print "---------------------------"
            
            # print project and sdk version
            print "PROJECT: " + self.project_name + " (" + self.project_path + ")"
            if self.sdk_version:
                print "SDK VERSION: " + self.sdk_version
        
            # print all languages
            print "---------------------------"
            if not current_point_of_sale:
                print " *  Start"
            for point_of_sale in self.points_of_sale.all_points_of_sale:
                prefix = "   "
                if current_point_of_sale == point_of_sale:
                    prefix = " * "
                comment = ""
                if point_of_sale['comment']:
                    comment = point_of_sale['comment']
                print "%s %-30s %-20s %s" % (prefix, point_of_sale["name"], (point_of_sale["language"] + "_" + point_of_sale["locale"]), comment)

            # print the commands
            print "---------------------------"
            print "COMMANDS:"
            if next_point_of_sale:
                print "\t <enter> \t\t\t test " + next_point_of_sale["name"] + " (" + next_point_of_sale['language'] + "_" + next_point_of_sale['locale'] + ")"
            if current_point_of_sale:
                print "\t c \t\t\t\t make a comment on this language"
            print "\t <pos-name | pos-code> \t\t jump to a specific point of sale"
            print "\t r \t\t\t\t print the current version of the report"
            print "\t e \t\t\t\t exit and print report"

            x = raw_input(">> ")

            # exit
            if x == "e":
                self.print_report()
                break
            
            # see report
            elif x == "r":
                self.print_report()
                print
                print
                x = raw_input("Hit <enter> to continue...")
            
            # comment
            elif x == "c" and current_point_of_sale:
                current_point_of_sale['comment'] = raw_input("Enter comment: ")
           
            # change language by code
            elif self.points_of_sale.point_of_sale_for_code(x):
                print "Building and running..."
                current_point_of_sale = self.points_of_sale.point_of_sale_for_code(x)
                simulator.set_point_of_sale(current_point_of_sale['language'], current_point_of_sale['locale'], self.sdk_version)
                simulator.build_and_run(self.project_path, self.project_name)
        
            # change language by name
            elif self.points_of_sale.point_of_sale_for_name(x):
                print "Building and running..."
                current_point_of_sale = self.points_of_sale.point_of_sale_for_name(x)
                simulator.set_point_of_sale(current_point_of_sale['language'], current_point_of_sale['locale'], self.sdk_version)
                simulator.build_and_run(self.project_path, self.project_name)
        
            # next language if applicable
            elif x == "":
                current_point_of_sale = next_point_of_sale
                if current_point_of_sale:
                    print "Building and running..."
                    simulator.set_point_of_sale(current_point_of_sale['language'], current_point_of_sale['locale'], self.sdk_version)
                    simulator.build_and_run(self.project_path, self.project_name)