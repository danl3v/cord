README
----------------------------------------

INTRODUCTION:
----------------------------------------
This tool helps to test apps in different languages and locales on the iOS simulator. It automates the
process of changing the devices language and locale allows you to take notes for each. It then prints
out a report at the end.

It is still a work in progress, but if you want to test it out, let me know at me@danl3v.com

RUNNING WITH COMMAND LINE ARGUMENTS:
----------------------------------------
Example for testing MyCoolApp:
python run_pos_tester.py -n MyCoolApp -p /Users/<your username>/MyCoolApp

RUNNING WITH CONFIG FILE:
----------------------------------------
Remembering command line arguments can be very annoying, so you can also specify a config file
that remembers your arguments for you. Here is an example config file, which is saved as mycoolapp.py:

project_name = "MyCoolApp"
project_path = "/Users/<your username>/MyCoolApp"

Then to run the tester, you type:
python run_pos_tester.py -c mycoolapp

So much easier! Feel free to have multiple config files for each app you test.

Note: Your config file must be in the same directory as this file.
