class PointsOfSale:
    def __init__(self):
        self.all_points_of_sale = self.get_points_of_sale()
        
        for point_of_sale in self.all_points_of_sale:
            point_of_sale['comment'] = None

    def get_points_of_sale(self):
        return [
                {"name" : "English Australia", "language" : "en", "locale" : "AU"},
                {"name" : "English Canada", "language" : "en", "locale" : "CA"},
                {"name" : "English Hong Kong", "language" : "en", "locale" : "HK"},
                {"name" : "English Ireland", "language" : "en", "locale" : "IE"},
                {"name" : "English India", "language" : "en", "locale" : "IN"},
                {"name" : "English Malaysia", "language" : "en", "locale" : "MY"},
                {"name" : "English New Zealand", "language" : "en", "locale" : "NZ"},
                {"name" : "English Phillapines", "language" : "en", "locale" : "PH"},
                {"name" : "English Singapore", "language" : "en", "locale" : "SG"},
                {"name" : "English UK", "language" : "en", "locale" : "UK"},
                {"name" : "English US", "language" : "en", "locale" : "US"},
                
                {"name" : "Spanish Argentina", "language" : "es", "locale" : "AR"},
                {"name" : "Spanish Spain", "language" : "es", "locale" : "ES"},
                {"name" : "Spanish Mexico", "language" : "es", "locale" : "MX"},
                
                {"name" : "Portuguese Brazil", "language" : "pt", "locale" : "BR"},
                
                {"name" : "Danish", "language" : "da", "locale" : "DK"},
                {"name" : "German Austria", "language" : "de", "locale" : "AT"},
                {"name" : "German Germany", "language" : "de", "locale" : "DE"},
                {"name" : "French Belgium", "language" : "fr", "locale" : "BE"},
                {"name" : "French Canada", "language" : "fr", "locale" : "CA"},
                {"name" : "French France", "language" : "fr", "locale" : "FR"},
               
                {"name" : "Italian", "language" : "it", "locale" : "IT"},
                {"name" : "Norweigan", "language" : "nb", "locale" : "NO"},
                {"name" : "Dutch Belgium", "language" : "nl", "locale" : "BE"},
                {"name" : "Dutch Netherlands", "language" : "nl", "locale" : "NL"},
                {"name" : "Swedish", "language" : "sv", "locale" : "SE"},
                
                {"name" : "Japanese", "language" : "ja", "locale" : "JP"},
                {"name" : "Korean", "language" : "ko", "locale" : "KR"},
                {"name" : "Bahasa Malaysia", "language" : "ms", "locale" : "MY"},
                {"name" : "Indonesian", "language" : "id", "locale" : "ID"},
                {"name" : "Russian", "language" : "ru", "locale" : "RU"},
                {"name" : "Thai", "language" : "th", "locale" : "TH"},
                {"name" : "Vietnamese", "language" : "vi", "locale" : "VN"},
                {"name" : "Chinese HK Simplified", "language" : "zh-Hans", "locale" : "HK"},
                {"name" : "Chinese HK Tradional", "language" : "zh-Hant", "locale" : "HK"},
                {"name" : "Chinese Taiwan", "language" : "zh-Hant", "locale" : "TW"},
                {"name" : "Chinese United States", "language" : "zh-Hans", "locale" : "US"},
                {"name" : "Chinese China", "language" : "zh-Hans", "locale" : "CN"},
            ]

    def point_of_sale_for_code(self, code):
        code = code.replace("-", "").replace("_", "").replace(" ", "").lower()
        for point_of_sale in self.all_points_of_sale:
            if code == (point_of_sale["language"].replace("-", "").replace("_", "").lower() + point_of_sale["locale"].replace("-", "").replace("_", "").lower()):
                return point_of_sale
        return None

    def point_of_sale_for_name(self, name):
        for point_of_sale in self.all_points_of_sale:
            if name.replace(" ", "").lower() == point_of_sale["name"].lower():
                return point_of_sale
        return None