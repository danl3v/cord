import os
import tester

from optparse import OptionParser

def parse_options():
    import pos
    posObject = pos.PointsOfSale()
    all_points_of_sale = posObject.get_points_of_sale()

    usage = "usage: %prog -v SDK_VERSION -n PROJECT_NAME -p PROJECT_PATH"
    description = """The purpose of this script is to aid with localization testing by building a project in each language and locale automatically.
The script will set the language and locale of the simulator and build and run the project for each language locale combination you want.
Here are currently supported languages: """ + ", ".join([pos["name"] + " (" + pos["language"] + "_" + pos["locale"] + ")" for pos in all_points_of_sale])
    
    parser = OptionParser(usage=usage, description=description)
    parser.add_option("-v", "--sdk", dest="sdk_version", metavar="SDK_VERSION", help="the sdk version to use")
    parser.add_option("-n", "--project_name", dest="project_name", metavar="PROJECT_NAME", help="the project name, e.g. FlightTrack")
    parser.add_option("-p", "--project_path", dest="project_path", metavar="PROJECT_PATH", help="the project path, e.g. " + os.environ['HOME'] + "/FlightTrack")
    parser.add_option("-c", "--config_file", dest="config_file", metavar="CONFIG_FILE", help="a config file containing the above parameters")
    
    (options, args) = parser.parse_args()
    
    return options

def main():
    options = parse_options()
    
    sdk_version = None
    project_name = None
    project_path = None
    
    try:
        module = __import__(options.config_file)
        try:
            sdk_version = module.sdk_version
        except:
        	sdk_version = None
        project_name = module.project_name
        project_path = module.project_path
    except:
        sdk_version = options.sdk_version
        project_name = options.project_name
        project_path = options.project_path

    the_tester = tester.Tester(sdk_version, project_name, project_path)
    the_tester.run()

if __name__ == "__main__":
    main()