import os, plistlib, re, string

def sdk_path():
    return os.environ['HOME'] + "/Library/Application Support/iPhone Simulator/"

def language_plist_path(sdk_version):
    return sdk_path() + sdk_version + "/Library/Preferences/.GlobalPreferences.plist"

def build_and_run(project_path, project_name):
    script = '''
    application "iPhone Simulator" quit
    tell application "Xcode"
        tell project "''' + project_name + '''"
            activate
            tell application "System Events"
                key code 15 using {command down}
            end tell
        end tell
    end tell
    application "iPhone Simulator" activate
    '''
    os.system("osascript -e '" + script + "'")
    
def set_point_of_sale_for_all_sdks(language, locale):
    sdks = valid_sdks()
    for sdk_version in sdks:
        set_point_of_sale(language, locale, sdk_version)
    
def set_point_of_sale(language, locale, sdk_version):
    if not sdk_version:
        set_point_of_sale_for_all_sdks(language, locale)
    else:
        plist_path = language_plist_path(sdk_version)
        plist_path_esc = plist_path.replace(" ", "\ ")
        os.system("cp " + plist_path_esc + " " + plist_path_esc + "~")
        os.system("plutil -convert xml1 " + plist_path_esc)
    
        plist = plistlib.readPlist(plist_path)
        languages =  plist['AppleLanguages']
        languages.remove(language)
        languages.insert(0, language)
    
        plist['AppleLocale'] = (language + "_" + locale)

        plistlib.writePlist(plist, plist_path)
    
        os.system("plutil -convert binary1 " + plist_path_esc)
        os.system("rm " + plist_path_esc + "~")
    
def set_sdk():
    script = '''
    application "iPhone Simulator" quit
    application "Xcode" quit
    '''
    os.system("osascript -e '" + script + "'")
    
    userstate_file = project_path + "/project.xcworkspace/xcuserdata/" + os.getlogin() + ".xcuserdatad/UserInterfaceState.xcuserstate"

    os.system("cp " + userstate_file + " " + userstate_file + "~")
    os.system("plutil -convert xml1 " + userstate_file + "~")
    
    old_plist = open(userstate_file + "~", "r")
    text = old_plist.read()
    old_plist.close()

    result = re.search("iPhoneSimulator([0123456789\.]*).sdk-(iPad|iPhone)", text)
    
    text = string.replace(text, result.group(0), "iPhoneSimulator" + ".".join(sdk_version.split(".")[:2]) + ".sdk-" + device)

    new_plist = open(userstate_file, "w")
    new_plist.write(text)
    new_plist.close()
    
    os.system("plutil -convert binary1 " + userstate_file)
    os.system("rm " + userstate_file + "~")


def valid_sdks():
    sdk_list = []
    if (os.path.isdir(sdk_path())):
        dirList = os.listdir(sdk_path())
        for fname in dirList:
            if fname[0] in ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]:
                sdk_list.append(fname)
    return sdk_list